import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from "./components/home/home.component";
import {WorkComponent} from "./components/work/work.component";
import {ContactComponent} from "./components/contact/contact.component";
import {AboutComponent} from "./components/about/about.component";

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    data: {animation: 'HomePage'}
  }, {
    path: 'about',
    component: AboutComponent,
    data: {animation: 'AboutPage'}
  }, {
    path: 'work',
    component: WorkComponent,
    data: {animation: 'WorkPage'}
  }, {
    path: 'contact',
    component: ContactComponent,
    data: {animation: 'ContactPage'}
  }, {
    path: '**',
    component: HomeComponent,
    data: {animation: 'HomePage'}
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
