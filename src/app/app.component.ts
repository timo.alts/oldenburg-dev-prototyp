import {Component, Inject, LOCALE_ID} from '@angular/core';
import {RouterOutlet} from "@angular/router";
import {slideInAnimation} from "./animations";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [
    slideInAnimation
  ]
})
export class AppComponent {
  title = 'oldenburg-dev';

  constructor(
    @Inject(LOCALE_ID) protected localeId: string,
  ) {
  }

  prepareRoute(outlet: RouterOutlet) {
    return outlet?.activatedRouteData?.['animation'];
  }
}
