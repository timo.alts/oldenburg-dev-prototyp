import {animate, animateChild, group, query, style, transition, trigger} from "@angular/animations";

const animationDuration = '200ms';
export const slideInAnimation =
  trigger('routeAnimations', [
    transition('HomePage => WorkPage', [
      style({position: 'relative'}),
      query(':enter, :leave', [
        style({
          position: 'absolute',
          top: 0,
          right: 0,
          width: '100%',
        })
      ]),
      query(':enter', [
        style({right: '-100%'})
      ]),
      query(':leave', animateChild()),
      group([
        query(':enter', [
          animate(animationDuration + ' ease-out', style({right: '0%'}))
        ]),
        query(':leave', [
          animate(animationDuration + ' ease-out', style({right: '100%'}))
        ])
      ]),
      query(':enter', animateChild()),
    ]),
    transition('HomePage => ContactPage', [
      style({position: 'relative'}),
      query(':enter, :leave', [
        style({
          position: 'absolute',
          top: 0,
          left: 0,
          width: '100%',
        })
      ]),
      query(':enter', [
        style({top: '-100%'})
      ]),
      query(':leave', animateChild()),
      group([
        query(':leave', [
          animate(animationDuration + ' ease-out', style({top: '100%'}))
        ]),
        query(':enter', [
          animate(animationDuration + ' ease-out', style({top: '0%'}))
        ])
      ]),
      query(':enter', animateChild()),
    ]),

    transition('WorkPage => HomePage', [
      style({position: 'relative'}),
      query(':enter, :leave', [
        style({
          position: 'absolute',
          top: 0,
          left: 0,
          width: '100%',
        })
      ]),
      query(':enter', [
        style({left: '-100%'})
      ]),
      query(':leave', animateChild()),
      group([
        query(':leave', [
          animate(animationDuration + ' ease-out', style({left: '100%'}))
        ]),
        query(':enter', [
          animate(animationDuration + ' ease-out', style({left: '0%'}))
        ])
      ]),
      query(':enter', animateChild()),
    ]),
    transition('ContactPage => HomePage', [
      style({position: 'relative'}),
      query(':enter, :leave', [
        style({
          position: 'absolute',
          bottom: 0,
          left: 0,
          width: '100%',
        })
      ]),
      query(':enter', [
        style({bottom: '-100%'})
      ]),
      query(':leave', animateChild()),
      group([
        query(':leave', [
          animate(animationDuration + ' ease-out', style({bottom: '100%'}))
        ]),
        query(':enter', [
          animate(animationDuration + ' ease-out', style({bottom: '0%'}))
        ])
      ]),
      query(':enter', animateChild()),
    ]),

    transition('WorkPage => ContactPage', [
      style({position: 'relative'}),
      query(':enter, :leave', [
        style({
          position: 'absolute',
          top: 0,
          left: 0,
          width: '100%',
        })
      ]),
      query(':enter', [
        style({top: '-100%'})
      ]),
      query(':leave', animateChild()),
      group([
        query(':leave', [
          animate(animationDuration + ' ease-out', style({top: '100%'}))
        ]),
        query(':enter', [
          animate(animationDuration + ' ease-out', style({top: '0%'}))
        ])
      ]),
      query(':enter', animateChild()),
    ]),
    transition('ContactPage => WorkPage', [
      style({position: 'relative'}),
      query(':enter, :leave', [
        style({
          position: 'absolute',
          bottom: 0,
          left: 0,
          width: '100%',
        })
      ]),
      query(':enter', [
        style({bottom: '-100%'})
      ]),
      query(':leave', animateChild()),
      group([
        query(':leave', [
          animate(animationDuration + ' ease-out', style({bottom: '100%'}))
        ]),
        query(':enter', [
          animate(animationDuration + ' ease-out', style({bottom: '0%'}))
        ])
      ]),
      query(':enter', animateChild()),
    ]),

    transition('HomePage => AboutPage', [
      style({position: 'relative'}),
      query(':enter, :leave', [
        style({
          position: 'absolute',
          bottom: 0,
          left: 0,
          width: '100%',
        })
      ]),
      query(':enter', [
        style({bottom: '-100%'})
      ]),
      query(':leave', animateChild()),
      group([
        query(':leave', [
          animate(animationDuration + ' ease-out', style({bottom: '100%'}))
        ]),
        query(':enter', [
          animate(animationDuration + ' ease-out', style({bottom: '0%'}))
        ])
      ]),
      query(':enter', animateChild()),
    ]),
    transition('ContactPage => AboutPage', [
      style({position: 'relative'}),
      query(':enter, :leave', [
        style({
          position: 'absolute',
          bottom: 0,
          left: 0,
          width: '100%',
        })
      ]),
      query(':enter', [
        style({bottom: '-100%'})
      ]),
      query(':leave', animateChild()),
      group([
        query(':leave', [
          animate(animationDuration + ' ease-out', style({bottom: '100%'}))
        ]),
        query(':enter', [
          animate(animationDuration + ' ease-out', style({bottom: '0%'}))
        ])
      ]),
      query(':enter', animateChild()),
    ]),
    transition('WorkPage => AboutPage', [
      style({position: 'relative'}),
      query(':enter, :leave', [
        style({
          position: 'absolute',
          bottom: 0,
          left: 0,
          width: '100%',
        })
      ]),
      query(':enter', [
        style({bottom: '-100%'})
      ]),
      query(':leave', animateChild()),
      group([
        query(':leave', [
          animate(animationDuration + ' ease-out', style({bottom: '100%'}))
        ]),
        query(':enter', [
          animate(animationDuration + ' ease-out', style({bottom: '0%'}))
        ])
      ]),
      query(':enter', animateChild()),
    ]),

    transition('AboutPage => ContactPage', [
      style({position: 'relative'}),
      query(':enter, :leave', [
        style({
          position: 'absolute',
          top: 0,
          left: 0,
          width: '100%',
        })
      ]),
      query(':enter', [
        style({top: '-100%'})
      ]),
      query(':leave', animateChild()),
      group([
        query(':leave', [
          animate(animationDuration + ' ease-out', style({top: '100%'}))
        ]),
        query(':enter', [
          animate(animationDuration + ' ease-out', style({top: '0%'}))
        ])
      ]),
      query(':enter', animateChild()),
    ]),
    transition('AboutPage => WorkPage', [
      style({position: 'relative'}),
      query(':enter, :leave', [
        style({
          position: 'absolute',
          top: 0,
          right: 0,
          width: '100%',
        })
      ]),
      query(':enter', [
        style({right: '-100%'})
      ]),
      query(':leave', animateChild()),
      group([
        query(':enter', [
          animate(animationDuration + ' ease-out', style({right: '0%'}))
        ]),
        query(':leave', [
          animate(animationDuration + ' ease-out', style({right: '100%'}))
        ])
      ]),
      query(':enter', animateChild()),
    ]),
    transition('AboutPage => HomePage', [
      style({position: 'relative'}),
      query(':enter, :leave', [
        style({
          position: 'absolute',
          top: 0,
          left: 0,
          width: '100%',
        })
      ]),
      query(':enter', [
        style({top: '-100%'})
      ]),
      query(':leave', animateChild()),
      group([
        query(':leave', [
          animate(animationDuration + ' ease-out', style({top: '100%'}))
        ]),
        query(':enter', [
          animate(animationDuration + ' ease-out', style({top: '0%'}))
        ])
      ]),
      query(':enter', animateChild()),
    ]),

  ]);
